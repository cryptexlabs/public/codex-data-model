import { CountryCode, LanguageCode } from "./types";

export interface LocaleI18nInterface {
  /**
   * ISO 639 alpha-1 language code
   * Every endpoint must be able to receive and pass along this property
   */
  language: LanguageCode;

  /**
   * ISO 3166 alpha-2 country code
   * Every endpoint must be able to receive and pass along this property
   */
  country: CountryCode;

  readonly i18n: string;
}
