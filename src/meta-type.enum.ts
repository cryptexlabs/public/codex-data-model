export enum CodexMetaTypeEnum {
  NA_HTTP_ERROR = "cryptexlabs.codex.http.error",
  NA_HTTP_SIMPLE = "cryptexlabs.codex.http.simple",
  NA_HTTP_HEALTHZ = "cryptexlabs.codex.http.healthz",
  NA_WEBSOCKET_ERROR = "cryptexlabs.codex.websocket.error",
  NA_WEBSOCKET_SIMPLE = "cryptexlabs.codex.websocket.simple",
  NA_WEBSOCKET_SUBSCRIPTIONS_UPDATED = "cryptexlabs.codex.websocket.subscriptions.updated",
  NA_WEBSOCKET_PING = "cryptexlabs.codex.websocket.ping",
}
