export interface WebsocketSubscriptionsUpdatedInterface {
    subscriptions: string[];
}
