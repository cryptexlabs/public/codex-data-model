import { CountryLanguageCombination } from "./locale";
export declare enum ApiOrderDirectionEnum {
    ASC = "asc",
    DESC = "desc"
}
export interface ApiQueryOptionsInterface {
    searchName?: string;
    search?: string;
    orderBy?: string;
    orderDirection?: ApiOrderDirectionEnum;
}
export interface ApiMetaHeadersInterface {
    "x-correlation-id": string;
    "accept-language": CountryLanguageCombination;
    "x-started": string;
    "x-created": string;
    "x-context-category": string;
    "x-context-id": string;
    "x-client-id": string;
    "x-client-name": string;
    "x-client-version": string;
    "x-client-variant": string;
    "x-now"?: string;
}
