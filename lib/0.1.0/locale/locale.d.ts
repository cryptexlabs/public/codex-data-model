import { CountryCode, LanguageCode } from "./types";
import { LocaleI18nInterface } from "./locale.i18n.interface";
export declare class Locale implements LocaleI18nInterface {
    language: LanguageCode;
    country: CountryCode;
    constructor(language: LanguageCode, country: CountryCode);
    get i18n(): string;
}
