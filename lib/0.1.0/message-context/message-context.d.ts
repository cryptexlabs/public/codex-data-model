import { MessageContextInterface } from "./message-context.interface";
export declare class MessageContext implements MessageContextInterface {
    category: string;
    id: string;
    constructor(category?: string, id?: string);
}
