"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageContext = void 0;
class MessageContext {
    constructor(category = "default", id = "none") {
        this.category = category;
        this.id = id;
    }
}
exports.MessageContext = MessageContext;
