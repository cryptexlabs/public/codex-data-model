import { MessageContextInterface } from "./message-context";
import { LocaleInterface } from "./locale";
import { MetaTimeInterface } from "./meta-time.interface";
import { CodexMetaTypeEnum } from "./meta-type.enum";
import { ClientInterface } from "./client.interface";
export interface MessageMetaInterface {
    correlationId: string;
    time: MetaTimeInterface;
    context: MessageContextInterface;
    client: ClientInterface;
    type: CodexMetaTypeEnum | string;
    schemaVersion: "0.1.0";
    locale: LocaleInterface;
}
