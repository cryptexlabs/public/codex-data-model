"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiOrderDirectionEnum = void 0;
var ApiOrderDirectionEnum;
(function (ApiOrderDirectionEnum) {
    ApiOrderDirectionEnum["ASC"] = "asc";
    ApiOrderDirectionEnum["DESC"] = "desc";
})(ApiOrderDirectionEnum = exports.ApiOrderDirectionEnum || (exports.ApiOrderDirectionEnum = {}));
